import Vue from 'vue';
import Vuex from 'vuex';

import { getCountries, getCities } from '@/services/cities';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    countries: [],
    cities: [],
    tasks: [],
  },
  getters: {
    tasks: ({ tasks }) => tasks,
  },
  mutations: {
    setCountries: (state, countries) => {
      state.countries = countries;
    },

    setCities: (state, cities) => {
      state.cities = cities;
    },

    setTask: (state, task) => {
      state.tasks.push(task);
    },

    editTask: (state, task) => {
      const index = state.tasks.findIndex((t) => t.id === task.id);
      state.tasks[index] = task;
    },
  },
  actions: {
    async fetchCountries({ commit }) {
      const [countries, err] = await getCountries();
      if (!err) commit('setCountries', countries);
    },

    async fetchCities({ commit }, country) {
      const [cities, err] = await getCities(country);
      if (!err) commit('setCities', cities);
    },

    setTask({ commit, getters }, task) {
      if (task) {
        commit('setTask', { ...task, id: getters.tasks.length });
      }
    },

    editTask({ commit }, task) {
      if (task) {
        commit('editTask', task);
      }
    },
  },
});
