export const getCities = async (country) => (
  fetch('https://countriesnow.space/api/v0.1/countries/cities', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      country,
    }),
  })
    .then((data) => data.json())
    .then((cities) => [cities.data, false])
    .catch((err) => [[], err])
);

export const getCountries = async () => (
  fetch('https://countriesnow.space/api/v0.1/countries')
    .then((data) => data.json())
    .then((countries) => [countries.data.map(({ country }) => country), false])
    .catch((err) => [[], err])
);
