import store from '@/store/index';

describe('Calendar.vue', () => {
  it('should add new reminder', () => {
    const newReminder = {
      note: 'Unit Test',
      date: '2022-06-04',
      time: '23:10',
      country: 'Brazil',
      city: 'Entre Rios de Minas',
      color: '#FAFAFA',
      id: 0,
    };

    store.dispatch('setTask', newReminder);

    expect(store.state.tasks[0]).toEqual(newReminder);
  });
});
