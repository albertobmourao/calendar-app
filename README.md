# Calendar App
This project includes the following features:
- List days ina a month under their specific weekday;
- Add new reminder to a specific day;
- Edit a reminder;
- See reminders listed for a day.

Also, it's possible to unit test the method that adds a reminder.

## Project installation
```
yarn install
```

## How to run the project
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```